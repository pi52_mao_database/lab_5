﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Lab5_Entity.Startup))]
namespace Lab5_Entity
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
